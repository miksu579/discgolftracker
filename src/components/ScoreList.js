import React from "react";
import ScoreItem from "./ScoreItem";

//maps data and creates ScoreItem components with pieces of data within them
class ScoreList extends React.Component {
  render() {
    return (
      <div className="scorelist">
        {this.props.scoreData.map(round => {
          return (
            <ScoreItem
              name={round[1].name}
              score={round[1].score}
              scoreFromPar={round[1].scoreFromPar}
              date={round[1].date.slice(0, 10)}
              key={round[1].roundID}
            ></ScoreItem>
          );
        })}
      </div>
    );
  }
}
export default ScoreList;
